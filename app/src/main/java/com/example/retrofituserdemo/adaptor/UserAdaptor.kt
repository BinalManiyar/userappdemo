package com.example.retrofituserdemo.adaptor

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.retrofituserdemo.R
import com.example.retrofituserdemo.network.model.UserModelItem
import com.example.retrofituserdemo.view.UserHomeFragment
import kotlinx.android.synthetic.main.users_list.view.*

class UserAdaptor(
) : RecyclerView.Adapter<UserAdaptor.ViewHolder>() {

    private var uList: List<UserModelItem> = ArrayList()

    fun setUser(uList: List<UserModelItem>) {
        this.uList = uList
        notifyDataSetChanged()
    }

    class ViewHolder(v: View) : RecyclerView.ViewHolder(v) {

        val userId = v.tvUserId!!
        val userFName = v.tvUserName!!
        val userEmail = v.tvEmail!!
        val rootView = v.userCardView!!
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.users_list, parent, false)
        )
    }

    override fun getItemCount() = uList.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.userId.text = uList[position].id.toString()
        holder.userFName.text = uList[position].name
        holder.userEmail.text = uList[position].email

    }
}