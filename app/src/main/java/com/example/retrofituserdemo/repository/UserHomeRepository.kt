package com.example.retrofituserdemo.repository

import android.app.Application
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.retrofituserdemo.network.BASE_URL
import com.example.retrofituserdemo.network.UserNetwork
import com.example.retrofituserdemo.network.model.UserModelItem
import com.google.gson.Gson
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.*
import retrofit2.converter.gson.GsonConverterFactory
import java.lang.Exception

class UserHomeRepository(val application: Application) {

    val showProgress = MutableLiveData<Boolean>()
    val usersList = MutableLiveData<List<UserModelItem>>()

    fun retreiveUsers() {

        showProgress.value = true

        val retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()


        val api = retrofit.create(UserNetwork::class.java)
        val call : Call<List<UserModelItem>> = api.getUsers()


        call.enqueue(object : Callback<List<UserModelItem>>{
            override fun onFailure(call: Call<List<UserModelItem>>, t: Throwable) {
                showProgress.value = false
                Log.i("get user api",t.localizedMessage)
                Toast.makeText(application, "Error while accessing api", Toast.LENGTH_SHORT).show()
            }

            override fun onResponse(
                call: Call<List<UserModelItem>>,
                response: Response<List<UserModelItem>>
            ) {
                Log.i("msg", "Response : ${Gson().toJson(response.body())}")
                usersList.value = response.body()
                showProgress.value = false
            }

        })

    }
}