package com.example.retrofituserdemo.view

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.retrofituserdemo.R
import com.example.retrofituserdemo.adaptor.UserAdaptor
import com.example.retrofituserdemo.viewModel.UserHomeViewModel
import kotlinx.android.synthetic.main.fragment_user_home.*

class UserHomeFragment() : Fragment() {

    private lateinit var viewModel: UserHomeViewModel
    private lateinit var adaptor: UserAdaptor

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_user_home, container, false)

        viewModel = ViewModelProvider(this.requireActivity()).get(UserHomeViewModel::class.java)

        viewModel.showProgress.observe(viewLifecycleOwner, Observer {
            if (it) {
                homeProgressBar.visibility = View.VISIBLE
            } else {
                homeProgressBar.visibility = View.GONE
            }

        })

        //Log.i("Users", viewModel.retrieveUsers().toString())

        viewModel.usersList.observe(viewLifecycleOwner, Observer {

            Log.i("msg", viewModel.usersList.value.toString())
            viewModel.retrieveUsers()
            adaptor.setUser(it)
        })

        adaptor = UserAdaptor()
        rvUsers.adapter = adaptor
        return view
    }
}