package com.example.retrofituserdemo.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.retrofituserdemo.network.model.UserModelItem
import com.example.retrofituserdemo.repository.UserHomeRepository

class UserHomeViewModel(application: Application): AndroidViewModel(application) {

    private val repository = UserHomeRepository(application)
    val showProgress: LiveData<Boolean>
    val usersList: LiveData<List<UserModelItem>>


    init {
        this.usersList = repository.usersList
        this.showProgress = repository.showProgress
    }

    fun retrieveUsers() {
        repository.retreiveUsers()
    }
}