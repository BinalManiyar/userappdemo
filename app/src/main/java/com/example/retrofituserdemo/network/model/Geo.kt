package com.example.retrofituserdemo.network.model

data class Geo(
    val lat: String,
    val lng: String
)