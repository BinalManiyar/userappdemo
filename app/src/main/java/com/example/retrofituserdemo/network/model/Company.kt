package com.example.retrofituserdemo.network.model

data class Company(
    val bs: String,
    val catchPhrase: String,
    val name: String
)