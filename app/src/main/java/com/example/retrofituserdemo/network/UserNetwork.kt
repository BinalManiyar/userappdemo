package com.example.retrofituserdemo.network

import com.example.retrofituserdemo.network.model.UserModelItem
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.GET

const val BASE_URL = "https://jsonplaceholder.typicode.com"

interface UserNetwork {

    @GET("/users")
    fun getUsers(): Call<List<UserModelItem>>
}